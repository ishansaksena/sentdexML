# Regression Introduction
# Sentdex - Youtube
# Video 2, 3

import quandl as q
import pandas as pd
import math as m
import numpy as np
# Cross validation: shuffle and split into testing and training data
from sklearn import preprocessing, model_selection, svm

from sklearn.linear_model import LinearRegression

import warnings
warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

# Pull in stock data from
df = q.get('WIKI/GOOG')

# Keep only the columns we want
#df = df[['Adj. Open', 'Adj. High', 'Adj. Low', 'Adj. Close', 'Adj. Volume']]

df['HL_PCT'] = (df['Adj. High'] - df['Adj. Close']) / df['Adj. Close'] * 100
df['PCT_change'] = (df['Adj. Close'] - df['Adj. Open']) / df['Adj. Open'] * 100

df = df[['Adj. Close', 'HL_PCT', 'PCT_change', 'Adj. Volume']]

# Show me what you goooooot
# print(df.head())

#V3 beginning
forecast_col = 'Adj. Close'

# Replace Non numbers without sacrificing data points
df.fillna(-99999, inplace=True)

# Predict 1% into the future. i.e. 1% of the duration that the dataset covers
forecast_out = int(m.ceil(0.01 * len(df)))

# Store future values alongside current prices
df['label'] = df[forecast_col].shift(-forecast_out)
df.dropna(inplace=True)

# Show me what you goooooot
# print(df.tail())

#V4 beginning

# Features
X = np.array(df.drop(['label'], 1))
y = np.array(df['label'])

# Preprocessing: scale to 0:1
X = preprocessing.scale(X)

X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)

clf = LinearRegression()
clf.fit(X_train, y_train)
accuracy = clf.score(X_test, y_test)

#print("Accuracy: ", accuracy)

#V5

X = X[:-forecast_out]
X_lately = X[-forecast_out:]

forecast_set = clf.predict(X_lately)
print(forecast_set, forecast_out, accuracy)


